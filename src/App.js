import React, { Component } from 'react';
import Ingredients from './components/Ingredients/Ingredients';
import Builder from './components/Burger/Burger';
import './App.css';

class App extends Component {
  state = {
      ingredients: [
          {name: 'Meat', count: 0},
          {name: 'Cheese', count: 0},
          {name: 'Salad', count: 0},
          {name: 'Bacon', count: 0}
      ],
      totalPrice: 20
  };

    addButton = fill => {
        const ingr = [...this.state.ingredients];
        let totalPrice = this.state.totalPrice;
        for (let i = 0; i < ingr.length; i ++) {
            if (ingr[i].name === fill.name) {
                ingr[i].count++;
                totalPrice += fill.price;
            }
        }
        this.setState({ingredients: ingr, totalPrice});
    };

    removeIngr = fill => {
        const ingr = [...this.state.ingredients];
        let totalPrice = this.state.totalPrice;
        for (let i = 0; i < ingr.length; i ++) {
            if (ingr[i].name === fill.name && ingr[i].count !== 0) {
                ingr[i].count--;
                totalPrice -= fill.price;
            }
        }
        this.setState({ingredients: ingr, totalPrice});
    };

  render() {
    return (
      <div className="App">
          <Ingredients className="block-1"
              addHandler = {(name) => this.addButton(name)}
              removeHandler = {(name) => this.removeIngr(name)}
              count={this.state.ingredients}
          />
          <Builder
              ingredients = {this.state.ingredients}
              totalPrice = {this.state.totalPrice}
          />
      </div>
    );
  }
}

export default App;
