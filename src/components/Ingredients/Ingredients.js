import React from 'react';
import './Ingredients.css'

let meatImage = 'https://previews.123rf.com/images/magone/magone1711/magone171100085/89285125-grilled-burger-meat-isolated-on-white-background.jpg';
let cheeseImage = 'https://www.organicauthority.com/.image/t_share/MTU5MzI5OTc3NjUzMzM5MzUw/shutterstock_288297071.jpg';
let saladImage = 'http://imhow.ru/wp-content/uploads/2018/01/kak-soxranit-listya-salata-na-zimu1.jpg';
let baconImage = 'https://www.hire-profile.com/wp-content/uploads/2018/10/bacon.jpeg';

let Ingredients = props => {

  const Ingredients = [
      {name: 'Meat', price: 50, image: meatImage},
      {name: 'Cheese', price: 20, image: cheeseImage},
      {name: 'Salad', price: 5, image: saladImage},
      {name: 'Bacon', price: 30, image: baconImage}
  ];
    return (
        <div className='flex'>
            {
                Ingredients.map((item, index) => {
                    return (
                       <div key={index} className="ingredients">
                           <button onClick={() => props.addHandler(item)}><img className="img" src={item.image}/></button>
                           <span className="name">{item.name}</span>
                           <span>X {props.count[index].count}</span>
                           <button onClick={() => props.removeHandler(item)}>Delete</button>
                       </div>
                   )
                })}
        </div>
    )
};

export default Ingredients;