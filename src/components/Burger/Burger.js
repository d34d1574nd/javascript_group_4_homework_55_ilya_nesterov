import React, {Fragment} from 'react';
import Ingredient from '../Ingredient/Ingredient';
import './Burger.css';

const Builder = props =>
    <Fragment>
        <div className="Burger">
            <div className="BreadTop">
                <div className="Seeds1"></div>
                <div className="Seeds2"></div>
            </div>
            {
                props.ingredients.map((ingredient, index) => {
                    return (
                        <Ingredient
                            key={index}
                            name={ingredient.name}
                            count={ingredient.count}
                        />
                    )
                })
            }
            <div className="BreadBottom"></div>
            <span>Total Price: {props.totalPrice}</span>
        </div>
    </Fragment>;


export default Builder;